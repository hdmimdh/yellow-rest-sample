package com.yellow.sample.utils;

import org.springframework.stereotype.Component;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
@Component
public class SwaggerUtils {

    public static final String BAD_REQUEST = "Bad request";
    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
    public static final String UNAUTHORIZED = "Unauthorized";
}
