package com.yellow.sample.service;

import com.yellow.sample.dto.LoginResponseDto;
import com.yellow.sample.dto.LoginUserDto;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public interface SecurityUserService {

    LoginResponseDto login(LoginUserDto loginUserDto);
}
