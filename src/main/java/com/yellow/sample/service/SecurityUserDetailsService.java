package com.yellow.sample.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public interface SecurityUserDetailsService extends UserDetailsService{
}
