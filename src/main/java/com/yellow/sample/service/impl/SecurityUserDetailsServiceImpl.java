package com.yellow.sample.service.impl;

import com.yellow.sample.dao.UserDao;
import com.yellow.sample.model.SecurityUser;
import com.yellow.sample.service.SecurityUserDetailsService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
@Service
public class SecurityUserDetailsServiceImpl implements SecurityUserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(SecurityUserDetailsServiceImpl.class);

    private UserDao userDao;

    public SecurityUser loadUserByUsername(String email) throws UsernameNotFoundException {

        if (StringUtils.isBlank(email)) {
            return null;
        }

        SecurityUser user = (SecurityUser) userDao.findByEmail(email);
        if (user == null || user.getState().equals("inactive")) {
            return null;
        }

        return user;
    }
}
