package com.yellow.sample.service.impl;

import com.yellow.sample.dto.LoginResponseDto;
import com.yellow.sample.dto.LoginUserDto;
import com.yellow.sample.dto.builder.LoginResponseDtoBuilder;
import com.yellow.sample.model.SecurityUser;
import com.yellow.sample.security.AuthorizedUserTokenHandler;
import com.yellow.sample.service.SecurityUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
@Service
public class SecurityUserServiceImpl implements SecurityUserService {

    @Autowired
    private AuthorizedUserTokenHandler handler;
    @Autowired
    private AuthenticationManager authenticationManager;

    public LoginResponseDto login(LoginUserDto loginUserDto) {

        String email = loginUserDto.getEmail().toLowerCase();
        String password = loginUserDto.getPassword();

        UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(email, password);
        Authentication auth = authenticationManager.authenticate(loginToken);
        if (auth != null) {
            return createLoginResponse((SecurityUser) auth.getPrincipal());
        }

        return null;
    }

    public LoginResponseDto createLoginResponse(SecurityUser user) {

        String userToken = handler.obtainTokenFromUser(user);
        return new LoginResponseDtoBuilder()
                .email(user.getEmail())
                .name(user.getEmail())
                .token(userToken)
                .build();
    }
}
