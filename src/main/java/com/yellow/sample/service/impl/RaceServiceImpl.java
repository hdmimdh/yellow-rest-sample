package com.yellow.sample.service.impl;

import com.yellow.sample.dao.RaceDao;
import com.yellow.sample.dao.impl.RaceDaoImpl;
import com.yellow.sample.model.Race;
import com.yellow.sample.service.RaceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
@Service
public class RaceServiceImpl implements RaceService {

    private static final Logger logger = LoggerFactory.getLogger(RaceDaoImpl.class);

    private RaceDao raceDao;

    public void setRaceDao(RaceDao raceDao) {
        this.raceDao = raceDao;
    }

    @Transactional
    public void insert(Race race) {

        logger.info("Inserting race...");
        if (race == null) {
            logger.error("Race can't be empty.");
            throw new IllegalArgumentException("Race can't be empty.");
        }

        raceDao.insert(race);
    }

    @Transactional
    public Race findById(Long id) {

        logger.info("Searching for a race...");
        if (id == null) {
            logger.error("Race id can't be empty.");
            throw new IllegalArgumentException("Race id can't be empty.");
        }

        return raceDao.findById(id);
    }

    @Transactional
    public List<Race> findAll() {

        logger.info("Searching for all races...");
        return raceDao.findAll();
    }

    @Transactional
    public void update(Race race) {

        logger.info("Updating race...");
        if (race == null) {
            logger.error("Race can't be empty.");
            throw new IllegalArgumentException("Race can't be empty.");
        }

        raceDao.update(race);
    }

    @Transactional
    public void delete(Long id) {

        logger.info("Deleting race...");
        if (id == null) {
            logger.error("Race can't be empty.");
            throw new IllegalArgumentException("Race id can't be empty.");
        }

        raceDao.delete(id);
    }
}
