package com.yellow.sample.service;

import com.yellow.sample.model.Race;

import java.util.List;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public interface RaceService {

    void insert(Race race);

    Race findById(Long id);

    List<Race> findAll();

    void update(Race race);

    void delete(Long id);
}
