package com.yellow.sample.security;

import com.yellow.sample.model.SecurityUser;
import org.json.simple.JSONObject;


/**
 * The Class AuthorizedUserTokenHandler to encode/decode an user token.
 */
public class AuthorizedUserTokenHandler extends AbstractGeneralTokenHandler {

    private static final Integer TOKEN_LENGTH_BYTES = 1000;

    /**
     * Creates the token for user.
     *
     * @param user the user
     * @return the token
     */
    public String obtainTokenFromUser(SecurityUser user) {

        JSONObject userTokenObject = createUserTokenObject(user);
        byte[] userBytes = fromObjectToBytesConvert(userTokenObject);
        byte[] hash = generateHmacFromBytes(userBytes);
        final StringBuilder token = new StringBuilder(TOKEN_LENGTH_BYTES);
        return token.append(encodeToBase32(userBytes)).append(SEPARATOR)
                .append(encodeToBase32(hash)).toString();
    }

    /**
     * Obtains user object from token without token validation.
     *
     * @param token the token
     * @return parsed user
     */
    public SecurityUser obtainUserFromToken(String token) {
        UserToken userToken = parseStringToken(token);
        return (SecurityUser) fromBytesToObjectConvert(SecurityUser.class, userToken.getUserBytes());
    }

    private JSONObject createUserTokenObject(SecurityUser user) {
        Long expiredDate = System.currentTimeMillis() + getTokenDuration();
        JSONObject tokenObject = new JSONObject();
        tokenObject.put("id", user.getId());
        tokenObject.put("email", user.getEmail());
        tokenObject.put("expiredDate", expiredDate);
        tokenObject.put("permissions", user.getPermissions());
        return tokenObject;
    }
}

