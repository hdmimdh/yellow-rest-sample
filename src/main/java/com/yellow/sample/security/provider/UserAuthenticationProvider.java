package com.yellow.sample.security.provider;

import com.yellow.sample.dao.UserDao;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public class UserAuthenticationProvider implements AuthenticationProvider {

    private UserDao userDao;

    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        String name = authentication.getName();
        String password = authentication.getCredentials() != null ? authentication.getCredentials().toString() : null;
        return null;
    }

    public boolean supports(Class<?> aClass) {
        return false;
    }
}
