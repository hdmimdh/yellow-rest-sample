package com.yellow.sample.security;

/**
 * The Class UserToken.
 */
public class UserToken {
    private byte[] userBytes;
    private byte[] hashBytes;

    /**
     * Instantiates a new user token.
     *
     * @param userBytes the user bytes
     * @param hashBytes the hash bytes
     */
    public UserToken(byte[] userBytes, byte... hashBytes) {
        this.userBytes = userBytes;
        this.hashBytes = hashBytes;
    }

    /**
     * Gets the user bytes.
     *
     * @return the user bytes
     */
    public byte[] getUserBytes() {
        return userBytes;
    }

    /**
     * Sets the user bytes.
     *
     * @param userBytes the new user bytes
     */
    public void setUserBytes(byte... userBytes) {
        this.userBytes = userBytes;
    }

    /**
     * Gets the hash bytes.
     *
     * @return the hash bytes
     */
    public byte[] getHashBytes() {
        return hashBytes;
    }

    /**
     * Sets the hash bytes.
     *
     * @param hashBytes the new hash bytes
     */
    public void setHashBytes(byte... hashBytes) {
        this.hashBytes = hashBytes;
    }
}

