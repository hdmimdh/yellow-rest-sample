package com.yellow.sample.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base32;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * The Class GeneralTokenHandler to represent token handling general logic.
 */
public abstract class AbstractGeneralTokenHandler {
    private static final Integer TOKEN_PARTS_COUNT = 2;
    private static final Integer USER_TOKEN_PART_INDEX = 0;
    private static final Integer HASH_TOKEN_PART_INDEX = 1;
    private static final String SEPARATOR_SPLITTER = "\\.";
    private static final String HMAC_ALGO = "HmacSHA256";
    protected static final String INVALID_TOKEN_MESSAGE = "Illegal token has been sent";
    protected static final String SEPARATOR = ".";
    protected static final String TOKEN_ENDING = "====";

    @Autowired
    private ObjectMapper objectMapper;

    //DEC secret mac key algorithm
    private String secretMacKey;

    private String tokenEncoding;

    private Long tokenDuration;

    /**
     * Gets the secret mac key.
     *
     * @return the secret mac key
     */
    public String getSecretMacKey() {
        return secretMacKey;
    }

    /**
     * Sets the secret mac key.
     *
     * @param secretMacKey the new secret mac key
     */
    public void setSecretMacKey(String secretMacKey) {
        this.secretMacKey = secretMacKey;
    }

    /**
     * Gets the token encoding.
     *
     * @return the token encoding
     */
    public String getTokenEncoding() {
        return tokenEncoding;
    }

    /**
     * Gets the token duration.
     *
     * @return the token duration
     */
    public Long getTokenDuration() {
        return tokenDuration;
    }

    /**
     * Sets the token duration.
     *
     * @param tokenDuration the new token duration
     */
    public void setTokenDuration(Long tokenDuration) {
        this.tokenDuration = tokenDuration;
    }

    /**
     * Sets the token encoding.
     *
     * @param tokenEncoding the token encoding
     */
    public void setTokenEncoding(String tokenEncoding) {
        this.tokenEncoding = tokenEncoding;
    }

    /**
     * Parses string token into two parts - user bytes and hash.
     *
     * @param token the token
     * @return parsed user
     */
    public UserToken parseStringToken(String token) {
        String[] tokenParts = token.split(SEPARATOR_SPLITTER);
        if (tokenParts.length == TOKEN_PARTS_COUNT) {
            String userTokenPart = tokenParts[USER_TOKEN_PART_INDEX];
            String hashTokenPart = tokenParts[HASH_TOKEN_PART_INDEX];
            if (isTokenHasValidSize(userTokenPart, hashTokenPart)) {
                byte[] userBytes = decodeFromBase32(userTokenPart + TOKEN_ENDING);
                byte[] hashBytes = decodeFromBase32(hashTokenPart + TOKEN_ENDING);
                return new UserToken(userBytes, hashBytes);
            }
        }
        throw new IllegalArgumentException(INVALID_TOKEN_MESSAGE);
    }

    /**
     * Checks if token is valid.
     *
     * @param token the token
     * @return true, if is token valid
     */
    public boolean isTokenValid(UserToken token) {
        byte[] userBytes = token.getUserBytes();
        byte[] encodedUserBytes = generateHmacFromBytes(userBytes);
        return Arrays.equals(encodedUserBytes, token.getHashBytes());
    }

    /**
     * Encode to base32.
     *
     * @param content the content
     * @return the string
     */
    public String encodeToBase32(byte... content) {
        Base32 base32 = new Base32(true);
        String stringContent = base32.encodeAsString(content);
        return stringContent.replace("=", "");
    }

    /**
     * Decode from base32.
     *
     * @param content the content
     * @return the byte[]
     */
    public byte[] decodeFromBase32(String content) {
        Base32 base32 = new Base32(true);
        return base32.decode(content);
    }

    /**
     * Generate hmac from bytes.
     *
     * @param content the content
     * @return the byte[]
     */
    public byte[] generateHmacFromBytes(byte... content) {
        byte[] hashBytes = null;
        try {
            Mac hmac = Mac.getInstance(HMAC_ALGO);
            hmac.init(new SecretKeySpec(decodeFromBase32(secretMacKey), HMAC_ALGO));
            hashBytes = hmac.doFinal(content);
        } catch (Exception e) {
            throw new IllegalStateException("failed to initialize HMAC: " + e.getMessage(), e);
        }
        return hashBytes;
    }

    /**
     * From bytes to user convert.
     *
     * @param clazz     the class to convert to
     * @param userBytes the user bytes
     * @return the security user
     */
    @SuppressWarnings("unchecked")
    public Object fromBytesToObjectConvert(Class clazz, byte... userBytes) {
        Object object = null;
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(userBytes);
            object = objectMapper.readValue(byteArrayInputStream, clazz);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return object;
    }

    /**
     * From object to bytes convert.
     *
     * @param object the object
     * @return the byte[]
     */
    public byte[] fromObjectToBytesConvert(Object object) {
        try {
            return objectMapper.writeValueAsBytes(object);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private boolean isTokenHasValidSize(String userTokenPart, String hashTokenPart) {
        return !StringUtils.isEmpty(userTokenPart) && !StringUtils.isEmpty(hashTokenPart);
    }
}
