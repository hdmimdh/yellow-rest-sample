package com.yellow.sample.dto;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public class LoginResponseDto {

    private String token;
    private String email;
    private String name;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
