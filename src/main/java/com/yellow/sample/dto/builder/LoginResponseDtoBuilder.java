package com.yellow.sample.dto.builder;

import com.yellow.sample.dto.LoginResponseDto;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public class LoginResponseDtoBuilder {

    private LoginResponseDto loginResponseDto;

    public LoginResponseDtoBuilder() {
        loginResponseDto = new LoginResponseDto();
    }

    public LoginResponseDtoBuilder token(String token) {
        loginResponseDto.setToken(token);
        return this;
    }

    public LoginResponseDtoBuilder email(String email) {
        loginResponseDto.setEmail(email);
        return this;
    }

    public LoginResponseDtoBuilder name(String name) {
        loginResponseDto.setName(name);
        return this;
    }

    public LoginResponseDto build() {
        return loginResponseDto;
    }
}
