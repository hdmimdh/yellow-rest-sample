package com.yellow.sample.filter;

import com.yellow.sample.model.SecurityUser;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

/**
 * Authentication Filter to check auth token in request header.
 */
@Component
public class AuthenticationFilter extends AbstractFilter {



    /**
     * Do filter.
     *
     * @param request  the request
     * @param response the response
     * @param chain    the chain
     * @throws IOException      Signals that an I/O exception has occurred.
     * @throws ServletException the servlet exception
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        super.doFilter(request, response, chain);
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        if (OPTIONS_METHOD_NAME.equals(httpServletRequest.getMethod())) {
            chain.doFilter(request, response);
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.setContentType(CONTENT_TYPE_FOR_OPTION);
        } else {
            String token = httpServletRequest.getHeader(AUTH_TOKEN_NAME);
            SecurityUser securityUser = authenticationService.getAuthenticatedUser(token);
            if (securityUser == null) {
                HttpServletResponse httpResponse = (HttpServletResponse) response;
                httpResponse.setStatus(SC_UNAUTHORIZED);
            } else {
                chain.doFilter(request, response);
            }
        }
    }
}