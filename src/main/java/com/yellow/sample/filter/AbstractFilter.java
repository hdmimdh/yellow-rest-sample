package com.yellow.sample.filter;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The Class CorsFilter to set cors headers for cross-domains maintenance.
 */
@Component
public class AbstractFilter extends GenericFilterBean {

    private static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_NAME = "Access-Control-Allow-Origin";
    private static final String ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_VALUE = "*";
    private static final String ACCESS_CONTROL_ALLOW_METHODS_HEADER_NAME = "Access-Control-Allow-Methods";
    private static final String ACCESS_CONTROL_ALLOW_METHODS_HEADER_VALUE = "POST, PUT, GET, OPTIONS, DELETE";
    private static final String ACCESS_CONTROL_ALLOW_HEADERS_HEADER_NAME = "Access-Control-Allow-Headers";
    private static final String ACCESS_CONTROL_ALLOW_HEADERS_HEADER_VALUE = "Content-Type, X-Auth-Token, Origin";
    protected static final String AUTH_TOKEN_NAME = "X-Auth-Token";
    protected static final String OPTIONS_METHOD_NAME = "OPTIONS";
    protected static final String ADMIN_ROLE = "Admin";
    protected static final String CONTENT_TYPE_FOR_OPTION = "application/vnd.sun.wadl+xml";

    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse,
     * javax.servlet.FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        httpResponse.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_NAME, ACCESS_CONTROL_ALLOW_ORIGIN_HEADER_VALUE);
        httpResponse.setHeader(ACCESS_CONTROL_ALLOW_METHODS_HEADER_NAME, ACCESS_CONTROL_ALLOW_METHODS_HEADER_VALUE);
        httpResponse.setHeader(ACCESS_CONTROL_ALLOW_HEADERS_HEADER_NAME, ACCESS_CONTROL_ALLOW_HEADERS_HEADER_VALUE);
    }
}
