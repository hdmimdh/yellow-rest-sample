package com.yellow.sample.filter;

import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * CorsFilter Filter to set cors into headers and omit auth then.
 */
@Component
public class CorsFilter extends AbstractFilter {

    /**
     * Do filter.
     *
     * @param request  the request
     * @param response the response
     * @param chain    the chain
     * @throws IOException                    Signals that an I/O exception has occurred.
     * @throws javax.servlet.ServletException the servlet exception
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        super.doFilter(request, response, chain);
        chain.doFilter(request, response);

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        if (OPTIONS_METHOD_NAME.equals(httpServletRequest.getMethod())) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            httpResponse.setContentType(CONTENT_TYPE_FOR_OPTION);
        }
    }
}
