package com.yellow.sample.dao.impl;

import com.yellow.sample.dao.UserDao;
import com.yellow.sample.model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
@Repository
public class UserDaoImpl implements UserDao {

    private static final Logger logger = LoggerFactory.getLogger(RaceDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public User findById(Long id) {

        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.load(User.class, new Long(id));
        logger.info("User loaded successfully, User details = " + user);
        return user;
    }

    public User findByEmail(String email) {

        Query query = sessionFactory.getCurrentSession().createQuery("from User where email=:email");
        query.setParameter("email", email);
        User user = (User) query.uniqueResult();
        logger.info("User loaded successfully, User details = " + user);

        return user;
    }
}
