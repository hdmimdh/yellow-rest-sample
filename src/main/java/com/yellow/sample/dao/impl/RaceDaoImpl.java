package com.yellow.sample.dao.impl;

import com.yellow.sample.dao.RaceDao;
import com.yellow.sample.model.Race;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
@Repository
public class RaceDaoImpl implements RaceDao {

    private static final Logger logger = LoggerFactory.getLogger(RaceDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf) {
        this.sessionFactory = sf;
    }

    public void insert(Race race) {

        Session session = this.sessionFactory.getCurrentSession();
        session.persist(race);
        logger.info("Race saved successfully, Race Details = " + race);
    }

    public Race findById(Long id) {

        Session session = this.sessionFactory.getCurrentSession();
        Race race = (Race) session.load(Race.class, new Long(id));
        logger.info("Race loaded successfully, Race details = " + race);
        return race;
    }

    public List<Race> findAll() {

        Session session = this.sessionFactory.getCurrentSession();
        List<Race> races = session.createQuery("from Race").list();
        logger.info("Races loaded successfully, Races details = " + races);

        return races;
    }

    public void update(Race race) {

        Session session = this.sessionFactory.getCurrentSession();
        session.update(race);
        logger.info("Race updated successfully, Race Details = " + race);
    }

    public void delete(Long id) {

        Session session = this.sessionFactory.getCurrentSession();
        Race race = (Race) session.load(Race.class, new Long(id));
        if (null != race) {
            session.delete(race);
        }
        logger.info("Race deleted successfully, race details=" + race);
    }
}
