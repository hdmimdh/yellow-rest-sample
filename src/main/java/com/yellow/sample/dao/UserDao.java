package com.yellow.sample.dao;

import com.yellow.sample.model.User;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public interface UserDao {

    User findById(Long id);

    User findByEmail(String email);
}
