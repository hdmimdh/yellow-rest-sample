package com.yellow.sample.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public class SecurityUser extends User implements UserDetails {

    private String username;
    private String password;
    private Long expiredDate;
    private Date resetPasswordSentDate;
    private boolean accountNonExpired = true;
    private boolean accountNonLocked = true;
    private boolean credentialsNonExpired = true;
    private boolean enabled = true;

    private List<String> permissions;

    /**
     * Instantiates a new user.
     */
    public SecurityUser() {
    }

    /**
     * Instantiates a new user.
     *
     * @param email    the email
     * @param password the password
     * @param state    the state
     */
    public SecurityUser(String email, String password, String state) {
        super.setEmail(email);
        this.password = password;
        setState(state);
    }

    public SecurityUser(String email) {
        super.setEmail(email);
    }

    /**
     * Gets the username.
     * Returns email, if username is null.
     *
     * @return the username
     */
    public String getUsername() {
        return username == null ? getEmail() : username;
    }

    /**
     * Sets the username.
     *
     * @param username the new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the password. Bcrypt algorithm is used.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password. Bcrypt algorithm is used
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the expiredDate.
     *
     * @return the expiredDate
     */
    public Long getExpiredDate() {
        return expiredDate;
    }

    /**
     * Sets the expiredDate.
     *
     * @param expiredDate the new expiredDate
     */
    public void setExpiredDate(Long expiredDate) {
        this.expiredDate = expiredDate;
    }

    /**
     * Gets the resetPasswordSentDate.
     *
     * @return the resetPasswordSentDate
     */
    public Date getResetPasswordSentDate() {
        return resetPasswordSentDate;
    }

    /**
     * Sets the resetPasswordSentDate.
     *
     * @param resetPasswordSentDate the date, when reset password notification was sent
     */
    public void setResetPasswordSentDate(Date resetPasswordSentDate) {
        this.resetPasswordSentDate = resetPasswordSentDate;
    }

    /**
     * Gets the permissions.
     *
     * @return the permissions
     */
    public List<String> getPermissions() {
        return permissions;
    }

    /**
     * Sets the permissions.
     */
    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    /**
     * Checks if is account non expired.
     *
     * @return true, if is account non expired
     */
    public boolean isAccountNonExpired() {
        return accountNonExpired;
    }

    /**
     * Checks if is account non locked.
     *
     * @return true, if is account non locked
     */
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    /**
     * Checks if is credentials non expired.
     *
     * @return true, if is credentials non expired
     */
    public boolean isCredentialsNonExpired() {
        return credentialsNonExpired;
    }

    /**
     * Checks if is enabled.
     *
     * @return true, if is enabled
     */
    public boolean isEnabled() {
        return enabled;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return new ArrayList<GrantedAuthority>();
    }
}
