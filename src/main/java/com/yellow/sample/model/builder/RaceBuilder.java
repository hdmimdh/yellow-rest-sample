package com.yellow.sample.model.builder;

import com.yellow.sample.model.Race;

import java.util.Date;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
public class RaceBuilder {

    private Race race;

    public RaceBuilder() {
        race = new Race();
    }

    public RaceBuilder id(Long id) {
        race.setId(id);
        return this;
    }

    public RaceBuilder distance(Long distance) {
        race.setDistance(distance);
        return this;
    }

    public RaceBuilder time(Long time) {
        race.setTime(time);
        return this;
    }

    public RaceBuilder date(Date date) {
        race.setDate(date);
        return this;
    }

    public Race build() {
        return race;
    }
}
