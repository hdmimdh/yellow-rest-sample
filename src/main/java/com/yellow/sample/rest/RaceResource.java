package com.yellow.sample.rest;

import com.yellow.sample.model.Race;
import com.yellow.sample.service.RaceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.yellow.sample.utils.SwaggerUtils.BAD_REQUEST;
import static com.yellow.sample.utils.SwaggerUtils.INTERNAL_SERVER_ERROR;
import static com.yellow.sample.utils.SwaggerUtils.UNAUTHORIZED;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
@RestController("/v1/yellow/races")
@Api(value = "/v1/yellow/races", description = "Web resource to work with races")
public class RaceResource {

    private static final Logger logger = LoggerFactory.getLogger(RaceResource.class);

    @Autowired
    private RaceService raceService;

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Creates new race.", httpMethod = "POST", response = Race.class)
    @ApiResponses({
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)})
    public ResponseEntity<Void> createRace(@RequestBody Race race) {

        raceService.insert(race);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Gets race by id.", httpMethod = "GET", response = Race.class)
    @ApiResponses({
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)})
    public ResponseEntity<Race> getRace(@PathVariable("id") Long id) {

        Race race = raceService.findById(id);
        if (race == null) {
            logger.debug("Race with id " + id + " not found");
            return new ResponseEntity<Race>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<Race>(race, HttpStatus.OK);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ApiOperation(value = "Gets all races.", httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)})
    public ResponseEntity<List<Race>> listAllRaces() {

        List<Race> races = raceService.findAll();
        if (CollectionUtils.isEmpty(races)) {
            return new ResponseEntity<List<Race>>(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<List<Race>>(races, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "Updates race by id.", httpMethod = "PUT", response = Race.class)
    @ApiResponses({
            @ApiResponse(code = SC_BAD_REQUEST, message = BAD_REQUEST),
            @ApiResponse(code = SC_UNAUTHORIZED, message = UNAUTHORIZED),
            @ApiResponse(code = SC_INTERNAL_SERVER_ERROR, message = INTERNAL_SERVER_ERROR)})
    public ResponseEntity<Race> updateRace(@RequestBody Race race, @PathVariable("id") Long id) {

        Race currentRace = raceService.findById(id);
        if (currentRace == null) {
            logger.debug(("Race with id " + id + " not found"));
            return new ResponseEntity<Race>(HttpStatus.NOT_FOUND);
        }

        raceService.update(race);
        return new ResponseEntity<Race>(race, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Race> deleteRace(@PathVariable("id") Long id) {

        Race race = raceService.findById(id);
        if (race == null) {
            logger.debug(("Race with id " + id + " not found"));
            return new ResponseEntity<Race>(HttpStatus.NOT_FOUND);
        }

        raceService.delete(id);
        return new ResponseEntity<Race>(HttpStatus.NO_CONTENT);
    }
}
