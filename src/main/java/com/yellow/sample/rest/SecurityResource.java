package com.yellow.sample.rest;

import com.yellow.sample.dto.LoginResponseDto;
import com.yellow.sample.dto.LoginUserDto;
import com.yellow.sample.service.SecurityUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by Uladzislau_Kuzmin on 2/4/2017.
 */
@RestController("/v1/yellow/auth")
public class SecurityResource {

    @Autowired
    private SecurityUserService securityUserService;

    @RequestMapping("/login")
    public LoginResponseDto login(@RequestBody @Valid LoginUserDto loginUserDto) {
        return securityUserService.login(loginUserDto);
    }
}
